import os
import pickle
import pandas as pd
import snowflake.connector
import teradatasql
import requests
from getpass import getpass
from string import Template

# from pyhive import hive


class DBConnector:
    """Grainger Database connnection, support 1. TeraData, 2. Snowflake
    Example:
    >>> from Extractor import DBConnector
    >>> connector = DBConnector()
    >>> df = connector.getSnowFlake("show tables;", database="AAD", schema=None, output_type="df")
    """

    def __init__(self):
        self.access_check()
        self.access_token = None

    def access_check(self):
        """Check whether you set the SAP_USERNAME and SAP_PASSWORD
        previously in the environment, otherwise type manually
        """
        try:
            self.username = os.environ["SAP_USERNAME"]
            self.passsword = os.environ["SAP_PASSWORD"]
        except KeyError:
            print(
                "You haven't set username and password as your environment variable, please type in manually"
            )
            print(
                "See toturial on how save the time passing user and passwords, see: https://bitbucket.grainger.com/profile",
                "\n",
            )

            self.username = getpass(prompt="User Name: ")
            self.passsword = getpass(prompt="Password: ")

    @staticmethod
    def sqlFileOpen(path):
        with open(path) as file:
            return file.read()

    def get_query(self, query_or_sqlfile, output_type, conn, **kwargs):
        if query_or_sqlfile.endswith(".sql", -4):
            query_or_sqlfile = DBConnector.sqlFileOpen(query_or_sqlfile)

        if kwargs:
            query_or_sqlfile = Template(query_or_sqlfile)
            query_or_sqlfile = query_or_sqlfile.substitute(kwargs)

        if output_type == "df":
            df = pd.read_sql(query_or_sqlfile, conn)
            return df
        elif output_type == "tuple":
            cur = conn.cursor()
            cur.execute(query_or_sqlfile)
            return cur.fetchall()
        else:
            raise (
                "The output type is either 'df' or 'tuple', please check your function args again"
            )

    def snowflake_token_helper(self):
        """get access token to snowflake"""
        if not self.access_token:
            try:
                request_data = {
                    "client_id": "snowflake",
                    "grant_type": "password",
                    "username": self.username,
                    "password": self.passsword,
                    "client_secret": "KmhZWlp6qM7agw4Ofe5ihiTITKsnOJilwKjjwxjMOMAFA7Bv5pxwThhJEclNot24",
                    "scope": "SESSION:ROLE-ANY",
                }
                resp = requests.post(
                    "https://pingf.grainger.com/as/token.oauth2",
                    data=request_data,
                )
                self.access_token = resp.json()["access_token"]
            except:
                raise (
                    "Please double check the input or you haven't get the permission to get the access_token"
                )

    def getSnowFlake(
        self,
        query_or_sqlfile,
        database="AAD",
        schema=None,
        output_type="df",
        **kwargs
    ):
        """Read SQL query or database table into a DataFrame from Teradata,
        support pd.dataframe and tuple type.
        Args:
            query_or_sqlfile: str or .sql file
            output_type: if specify 'df' then return pd.dataframe; return tuple
            if 'tuple'
        Return:
            df: return pd.dataframe or tuple according to the setting above
        """
        self.snowflake_token_helper()
        with snowflake.connector.connect(
            account="wwgrainger.us-east-1",
            authenticator="oauth",
            token=self.access_token,
            role="AAD_SVC",
            warehouse="AAD_WH_M",
            database=database,
            schema=schema,
        ) as conn:
            return self.get_query(
                query_or_sqlfile, output_type, conn, **kwargs
            )

    def getTeraData(self, query_or_sqlfile, output_type="df", **kwargs):
        """Read SQL query or database table into a DataFrame from Teradata,
        support pd.dataframe and tuple type.
        Args:
            query_or_sqlfile: str or .sql file
            output_type: if specify 'df' then return pd.dataframe; return tuple
            if 'tuple'
        Return:
            df: return pd.dataframe or tuple according to the setting above
        """
        with teradatasql.connect(
            host="prtrdatacop1.sap.grainger.com",
            user=self.username,
            password=self.passsword,
            logmech="LDAP",
        ) as conn:

            return self.get_query(
                query_or_sqlfile, output_type, conn, **kwargs
            )
