# Docker with AWS ECR

Currently, we upload images to AWS ECR for our development and commonly use `AWS CLI` to upload your image. The only difference is that everyone is designed to use the same AWS ECR repository called `709741256416.dkr.ecr.us-east-2.amazonaws.com`. Then we use the a image name(need to register the repo from MLOps team) let's say `aad-my-image-repo` to differentiate each user. Finally, we use the tag to specify the difference between images instead as weel as the verion number at same place. For example:

``` Bash
# Usually the image naming convention on ECR is
# <your_aws_account_id>.dkr.ecr.us-east-2.amazonaws.com/<image_name>:<version_number>
$ 709741256416.dkr.ecr.us-east-2.amazonaws.com/python_image:v1

# Instead, it is set to this format
# 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-myimage:<image_name>
$ 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-myimage:python_api_image

```

## To start using AWS ECR for Docker operations

There are two ways to push your image in general, one is push from local. It is easier but with slower upload speed. Second way is to push to repo that connect with CircleCI's CI/CD pipeline. The second option has faster upload speed but harder to setup since it needs a config yaml file to run CI/CD on CircleCI.

### Example 1: **Build Locally**

**For more information see [link](https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html)**

1. Create a docker image and make sure you have a repo in ECR. Contact MLOps team if you need one.
2. Build the image with a tag name specified

    `docker build -t python_image:v1 .`

3. Authenticate to your default registry (AWS CLI)

    `aws ecr get-login-password --region us-east-2 --profile prod | docker login --username AWS --password-stdin 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo`

4. Tag the image

    `docker tag python_image:v1 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:flask_v1`

5. Push an image to Amazon ECR

    `docker push 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:flask_v1`

### Example 2: **Build using CI/CD**

1. Create a docker image and make sure you have a repo in ECR. Contact MLOps team if you need one.
2. Build the image with a tag name specified

    `docker build -t python_image:v1 .`

3. Authenticate to your default registry (AWS CLI)

    `aws ecr get-login-password --region us-east-2 --profile prod | docker login --username AWS --password-stdin 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo`

4. Tag the image

    `docker tag python_image:v1 709741256416.dkr.ecr.us-east-1.amazonaws.com/aad-my-image-repo:python_image`

5. Write CI/CD pipeline for CircleCI

6. Commit the repo and let CircleCI connect the repo

7. CircleCI will test then push for you

## **Docker Example: RShiny App**

---

1. Select the image and the version
    * Rshiny: <https://hub.docker.com/r/rocker/shiny>
    * Rshiny + Tidyverse: <https://hub.docker.com/r/rocker/shiny-verse>

2. Pull from Docker Hub

    `docker pull rocker/shiny-verse:<version_tag>`

3. Run Docker and remember to mount the value to your project

    ```Docker
    docker run --rm -d -p 3838:3838 \
        -v /srv/shinyapps/:/srv/shiny-server/ \
        -v /srv/shinylog/:/var/log/shiny-server/ \
        rocker/shiny
    ```
