# Tutorials For Grainger MLOps

Here are a set of tutorials to understand how to use the resource effectively on Grainger's MLOps. The tutorial includes Introduction on all tools, Kubernetes, Airflow, Interact with AWS S3, Connect DB in Kubeflow/Kubernetes/Airflow

## How to try this tutorial

---
I recommend you to clone this repo in your kubeflow so that you can get most up to date version without manually download the files. [See how to clone my repo video](https://grainger-my.sharepoint.com/:v:/p/yen-chen_chou/EalRcUYUjxRGmYpi0QHPnOIBBkzqDR938hVTTvTRjghWmQ?e=FU1oQv)

## Available tutorials

---

1. **Introduction on all tools**
    * Slides: [Grainger Data Science Resource Introduction.pptx](https://bitbucket.grainger.com/users/xyxc025/repos/mlops/browse/Slides)
    * Videos:
        * [Onboard_Introduction.mp4](https://grainger-my.sharepoint.com/personal/yen-chen_chou_grainger_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fyen%2Dchen%5Fchou%5Fgrainger%5Fcom%2FDocuments%2FMLOps%5FTutorial%2FOnboard%5FIntroduction%2Emp4&parent=%2Fpersonal%2Fyen%2Dchen%5Fchou%5Fgrainger%5Fcom%2FDocuments%2FMLOps%5FTutorial)

2. **Interact with AWS S3**
    * Videos for Python:
        * [R S3 Videos in Jupyter Notebook](https://grainger-my.sharepoint.com/:v:/p/yen-chen_chou/EWJMUnom4blNnbQIUuU0X4YBQHbUZbkGMwtP1g_IvxKaxw?e=eMohbU)
        * [R S3 Videos in R Studio] - under development, there is a set up loading Error in R Studio, will be back soon! But if you use R in remote vscode it works.

    * Notebooks for Python
        * [Python Notebooks](https://bitbucket.org/yenchenchou/mlops/src/master/AWS_Resource_Interaction/Python/AWS_Boto3_Tutorial.ipynb)
    * Scripts/Notebooks for R
        * [R in Rstudio](https://bitbucket.org/yenchenchou/mlops/src/master/AWS_Resource_Interaction/R/AWS_paws_Tutorial_R.R)
            * Please refer to [`S3Helper.R`](https://bitbucket.org/yenchenchou/mlops/src/master/AWS_Resource_Interaction/R/S3Helper.R) for function details
        * [R in Jupyter Notebooks](https://bitbucket.org/yenchenchou/mlops/src/master/AWS_Resource_Interaction/R/AWS_Boto3_Tutorial_R.ipynb)

3. **Access databases with your Kubeflow (snowflake, teradata, ads)**
    * [Python Notebooks](https://bitbucket.org/yenchenchou/mlops/src/master/Connect_DB/Python/Database_Extractor_Examples.ipynb)
    * R Markdown - under development

## Support from MLOps Team

---
The MLOps team created a brief tutorial as well. Hence, this tutorial is more like a complement to fill the gaps between their instruction. Feel free to check out their tutorials as well. [See Link from MLOps](https://confluence.grainger.com/display/MLOPS/Machine+Learning+Operations+Home)

## Upcoming tutorials

---

1. Kubeflow Introduction
2. Airflow Introduction
3. Kubernetes Introduction

## Current Images on AWS ECR

---

1. FastAPI base
    * Image Name: 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:fastapi_v21
    * Meta: The push refers to repository [709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo]
    * digest ID:
    * Size:
    * Description: base image to deploy FastAPI using uvicorn
    * Date: 04/08/2021

2. FastAPI keepstock
    * Image Name: 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:keepstockapi_image:v8
    * Meta: The push refers to repository [709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo]
    * digest ID:
    * Size:
    * Description: keepstock image with model file (.pkl) to deploy FastAPI using uvicorn
    * Date: 04/09/2021
    * Link: <https://keepstockapi-v8.aad.prodaws.grainger.com/docs#/default/predict_species_predict_post>
