aws ecr get-login-password --region us-east-2 --profile prod | docker login --username AWS --password-stdin 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo

docker tag fastapi:v23 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:fastapi_v23

docker push 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:fastapi_v23
